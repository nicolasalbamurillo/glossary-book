package org.fjala.book.glossary;

import java.util.List;

public class GlossaryTerm implements Term<GlossaryTermValue> {
    private final String term;
    private GlossaryTermValue value;

    public GlossaryTerm(String name, String definition, List<String> examples) {
        this.term = name;
        this.value = new GlossaryTermValue(definition, examples);
    }

    public String getTerm() {
        return term;
    }

    @Override
    public GlossaryTermValue getValue() {
        return value;
    }

    @Override
    public void addTermValue(GlossaryTermValue glossaryTermValue) {
        glossaryTermValue.getExamples().addAll(glossaryTermValue.getExamples());
    }

    public String getDefinition() {
        return value.getDefinition();
    }

    public void setDefinition(String definition) {
        this.value.setDefinition(definition);
    }

    @Override
    public String toString() {
        return term + ": " + value;
    }

    public List<String> getExamples() {
        return value.getExamples();
    }
}
