package org.fjala.book.glossary;

public class Book {

    private final String name;
    private final BookIndex index;
    private final Glossary glossary;

    public Book(String name) {
        this.name = name;
        this.index = new BookIndex();
        this.glossary = new Glossary();
    }

    public void printBook() {
        System.out.println(name);
        index.printTerms();
        glossary.printTerms();
    }
}
