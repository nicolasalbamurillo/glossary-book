package org.fjala.book.glossary;

/**
 * A representation of a Term and its value.
 *
 * @param <TermValue> Can be multiples params in one class.
 */
public interface Term<TermValue> {
    String getTerm();

    TermValue getValue();

    /**
     * Add other term values in this term.
     */
    void addTermValue(TermValue value);
}
