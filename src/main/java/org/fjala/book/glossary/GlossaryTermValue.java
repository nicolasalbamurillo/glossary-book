package org.fjala.book.glossary;

import java.util.Iterator;
import java.util.List;

public class GlossaryTermValue {
    private String definition;
    private List<String> examples;

    public GlossaryTermValue(String definition, List<String> examples) {
        this.definition = definition;
        this.examples = examples;
    }

    public List<String> getExamples() {
        return examples;
    }

    public String getDefinition() {
        return definition;
    }

    public void setDefinition(String definition) {
        this.definition = definition;
    }

    public void setExamples(List<String> examples) {
        this.examples = examples;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append(definition);
        if (examples != null && !examples.isEmpty()) {
            sb.append(" Examples: ");
            Iterator<String> iterator = examples.iterator();
            sb.append(iterator.next());
            while(iterator.hasNext()) {
                sb.append(", ");
                sb.append(iterator.next());
            }
        }
        return sb.toString();
    }
}
