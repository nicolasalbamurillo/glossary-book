package org.fjala.book.glossary;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class BookIndexTerm implements Term<List<Integer>> {

    private final String name;
    private final List<Integer> pages;

    public BookIndexTerm(String name, int page) {
        this.name = name;
        pages = new ArrayList<Integer>();
        pages.add(page);
    }

    public BookIndexTerm(String name, List<Integer> pages) {
        this.name = name;
        this.pages = pages;
    }

    private String pagesToString() {
        Collections.sort(pages);

        int previousPage = pages.get(0);
        int countPages = 0;
        StringBuilder pagesSorted = new StringBuilder(String.valueOf(previousPage));

        for (int i = 1; i < pages.size(); i++) {
            int currentPage = pages.get(i);
            if (currentPage - previousPage > 1) {
                if (countPages > 0) {
                    pagesSorted.append(" - ");
                    pagesSorted.append(previousPage);
                }
                pagesSorted.append(", ");
                pagesSorted.append(currentPage);

                countPages = 0;
            } else {
                countPages++;
            }
            previousPage = currentPage;
        }

        if (countPages > 0) {
            pagesSorted.append(" - ");
            pagesSorted.append(previousPage);
        }
        return pagesSorted.toString();
    }

    @Override
    public String toString() {
        return name + ": " + pagesToString();
    }

    @Override
    public String getTerm() {
        return name;
    }

    @Override
    public List<Integer> getValue() {
        return pages;
    }

    @Override
    public void addTermValue(List<Integer> integers) {
        pages.addAll(integers);
    }

    public List<Integer> getPages() {
        return pages;
    }
}
