package org.fjala.book.glossary.redblacktree;


public class RedBlackNode<T, K extends Comparable<K>> {
	protected T value;
	protected K key;
	protected Color color;
	protected RedBlackNode<T, K> left;
	protected RedBlackNode<T, K> right;
	protected RedBlackNode<T, K> parent;
	
	public RedBlackNode(T value, K key, RedBlackNode<T, K> parent) {
		this.value = value;
		this.key = key;
		this.parent = parent;
		this.color = Color.BLACK;
	}

	public boolean isRed() {
		return color == Color.RED;
	}

	public boolean isBlack() {
		return color == Color.BLACK;
	}
	
	public T getValue() {
		return value;
	}
}
