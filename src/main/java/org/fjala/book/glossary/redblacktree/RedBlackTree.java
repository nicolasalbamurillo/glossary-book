package org.fjala.book.glossary.redblacktree;

import java.util.ArrayList;
import java.util.List;

public class RedBlackTree<T, K extends Comparable<K>> {

	private RedBlackNode<T, K> root;
	private RedBlackNode<T, K> nil;

	public RedBlackTree(RedBlackNode<T, K> nil) {
		this.root = nil;
		this.nil = nil;
	}

	public void insert(RedBlackNode<T, K> node) {
		root = insert(root, node, nil);
		fixAfterInsert(node);
	}
	
	public void insert(T value, K key) {
		RedBlackNode<T, K> node = new RedBlackNode<T, K>(value, key, nil);
		root = insert(root, node, nil);
		fixAfterInsert(node);
	}

	private RedBlackNode<T, K> insert(RedBlackNode<T, K> tree, RedBlackNode<T, K> node, RedBlackNode<T, K> parent) {
		if (tree == nil) {
			node.parent = parent;
			node.left = nil;
			node.right = nil;
			node.color = Color.RED;
			return node;
		}
		
		int cmp = node.key.compareTo(tree.key);
		if (cmp < 0) {
			tree.left = insert(tree.left, node, tree);
		} else {
			tree.right = insert(tree.right, node, tree);
		}
		return tree;
	}
	
	private void fixAfterInsert(RedBlackNode<T, K> x) {
		while(x.parent.isRed()) {
			if (x.parent == x.parent.parent.left) {
				RedBlackNode<T, K> y = x.parent.parent.right;
				if (y.isRed()) {
					x.parent.color = Color.BLACK;
					y.color = Color.BLACK;
					x.parent.parent.color = Color.RED;
					x = x.parent.parent;
				} else {
					if (x == x.parent.right) {
						x = x.parent;
						rotateLeft(x);
					}
					x.parent.color = Color.BLACK;
					x.parent.parent.color = Color.RED;
					rotateRight(x.parent.parent);
				}
			} else {
				RedBlackNode<T, K> y = x.parent.parent.left;
            	if (y.isRed()) {
            		x.parent.color = Color.BLACK;
            		y.color = Color.BLACK;
            		x.parent.parent.color = Color.RED;
            		x = x.parent.parent;
                } else {
                    if (x == x.parent.left) {
                        x = x.parent;
                        rotateRight(x);
                    }
                    x.parent.color = Color.BLACK;
                    x.parent.parent.color = Color.RED;
                    rotateLeft(x.parent.parent);
                }
			}
		}
		root.color = Color.BLACK;
	}
	
	private void rotateLeft(RedBlackNode<T, K> x) {
		RedBlackNode<T, K> y = x.right;
		x.right = y.left;
		if (y.left != nil) {
			y.left.parent = x;
		}
		y.parent = x.parent;
		if (x.parent == nil) {
			root = y;
		} else if (x.parent.left == x) {
			x.parent.left = y;
		} else {
			x.parent.right = y;
		}
		y.left = x;
		x.parent = y;
	}

	private void rotateRight(RedBlackNode<T, K> x) {
		RedBlackNode<T, K> y = x.left;
		x.left = y.right;
		if (y.right != nil) {
			y.right.parent = x;
		}
		y.parent = x.parent;
		if (x.parent == nil)
			root = y;
		else if (x.parent.right == x)
			x.parent.right = y;
		else
			x.parent.left = y;
		y.right = x;
		x.parent = y;
	}

	private RedBlackNode<T, K> min(RedBlackNode<T, K> x) {
		if (x.left == nil)
			return x;
		return min(x.left);
	}

	public RedBlackNode<T, K> get(K key) {
        return getNode(root, key);
    }

	private RedBlackNode<T, K> getNode(RedBlackNode<T, K> x, K key) {
		if (x == nil)
			return null;
		int cmp = key.compareTo(x.key);
		if (cmp < 0)
			return getNode(x.left, key);
		else if (cmp > 0)
			return getNode(x.right, key);
		else
			return x;
	}
	
	private void transplant(RedBlackNode<T, K> x, RedBlackNode<T, K> y) {
		if (x.parent == nil) {
			root = y;
		} else if (x == x.parent.left) {
			x.parent.left = y;
		} else {
			x.parent.right = y;
		}
		y.parent = x.parent;
	}

	public void delete(K key) {
		RedBlackNode<T, K> node = getNode(root, key);
		if (node != null) {
			delete(node);
		}
	}
	
	private void delete(RedBlackNode<T, K> x) {
		RedBlackNode<T, K> y = x;
		RedBlackNode<T, K> z;
		Color yPreviousColor = y.color;
		if (x.left == nil) {
			z = x.right;
			transplant(x, x.right);
		} else if (x.right == nil) {
			z = x.left;
			transplant(x, x.left);
		} else {
			y = min(x.right);
			yPreviousColor = y.color;
			z = y.right;
			if (y.parent == x) {
				z.parent = y;
			} else {
				transplant(y, y.right);
				y.right = x.right;
				y.right.parent = y;
			}
			transplant(x, y);
			y.left = x.left;
			y.left.parent = y;
			y.color = x.color;
		}

		if (yPreviousColor == Color.BLACK)
			fixAfterDelete(z);
	}

	private void fixAfterDelete(RedBlackNode<T, K> x) {
		while (x != root && x.isBlack()) {
            if (x == x.parent.left) {
            	RedBlackNode<T, K> w = x.parent.right;
                if (w.isRed()) {
                	w.color = Color.BLACK;
                	x.parent.color = Color.RED;
                	rotateLeft(x.parent);
                	w = x.parent.right;
                }

                if (w.left.isBlack() && w.right.isBlack()) {
                	w.color = Color.RED;
                	x = x.parent;
                } else {
                    if (w.right.isBlack()) {
                    	w.left.color = Color.BLACK;
                    	w.color = Color.RED;
                    	rotateRight(w);
                    	w = x.parent.right;
                    }
                    w.color = x.parent.color;
                    x.parent.color = Color.BLACK;
                    w.right.color = Color.BLACK;
                    rotateLeft(x.parent);
                    x = root;
                }
            } else {
            	RedBlackNode<T, K> w = x.parent.left;
            	if (w.isRed()) {
                	w.color = Color.BLACK;
                	x.parent.color = Color.RED;
                	rotateRight(x.parent);
                	w = x.parent.left;
                }
            	
            	if (w.right.isBlack() && w.left.isBlack()) {
            		w.color = Color.RED;
            		x = x.parent;
            	} else {
            		if (w.left.isBlack()) {
            			w.right.color = Color.BLACK;
                    	w.color = Color.RED;
						rotateLeft(w);
						w = x.parent.left;
					}
					w.color = x.parent.color;
					x.parent.color = Color.BLACK;
					w.left.color = Color.BLACK;
					rotateRight(x.parent);
					x = root;
				}

			}
		}
		x.color = Color.BLACK;
	}

	public List<T> inorder() {
		return inorder(root);
	}
	
	private List<T> inorder(RedBlackNode<T, K> x) {
		List<T> inorderList = new ArrayList<>();
		if (x.left != nil) {
			inorderList.addAll(inorder(x.left));
		}

		inorderList.add(x.value);

		if (x.right != nil) {
			inorderList.addAll(inorder(x.right));
		}
		return inorderList;
	}
}

