package org.fjala.book.glossary.redblacktree;

public enum Color {
	BLACK,
	RED;
}
