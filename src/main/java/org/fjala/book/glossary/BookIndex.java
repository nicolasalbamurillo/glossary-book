package org.fjala.book.glossary;

import java.util.ArrayList;
import java.util.List;

public class BookIndex extends Dictionary<BookIndexTerm, List<Integer>> {

    @Override
    public BookIndexTerm createInstance(String name, List<Integer> integers) {
        return new BookIndexTerm(name, integers);
    }

    public void addTerm(String term, int page) {
        List<Integer> list = new ArrayList<>();
        list.add(page);
        this.addTerm(term, list);
    }
}
