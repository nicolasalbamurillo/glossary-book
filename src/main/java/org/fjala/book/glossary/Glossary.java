package org.fjala.book.glossary;

import java.util.Collection;
import java.util.LinkedList;

public class Glossary extends Dictionary<GlossaryTerm, GlossaryTermValue> {
    public void addTerm(String term, String definition) {
        addTerm(term, new GlossaryTermValue(definition, new LinkedList<>()));
    }

    public void addExample(String term, String example) {
        if (!hasTerm(term)) {
            throw new IllegalArgumentException("The glossary does not contain the term: " + term);
        }
        getExamples(term).add(example);
    }

    public Collection<String> getExamples(String term) {
        return terms.get(term).getValue().getExamples();
    }

    @Override
    public GlossaryTerm createInstance(String name, GlossaryTermValue glossaryTermValue) {
        return new GlossaryTerm(name, glossaryTermValue.getDefinition(), glossaryTermValue.getExamples());
    }

    @Override
    public String toString() {
        return super.toString();
    }

    public void updateDefinition(String term, String definition) {
        getTerm(term).setDefinition(definition);
    }
}
