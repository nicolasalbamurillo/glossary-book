package org.fjala.book.glossary;

import org.fjala.book.glossary.redblacktree.RedBlackNode;
import org.fjala.book.glossary.redblacktree.RedBlackTree;

import java.util.List;
import java.util.stream.Collectors;

/**
 * An abstract representation of a Dictionary.
 *
 * @param <TermType> It's the Term Type.
 * @param <TermValue> It's the Term Value used in Term Type.
 */
public abstract class Dictionary<TermType extends Term<TermValue>, TermValue> {

    protected RedBlackTree<TermType, String> terms;

    public Dictionary() {
        terms = new RedBlackTree<>(new RedBlackNode<TermType, String>(null, null, null));
    }

    public void addTerm(String name, TermValue value) {
        RedBlackNode<TermType, String> termNode = terms.get(name);
        if (termNode != null) {
            TermType bookIndexTerm = termNode.getValue();
            bookIndexTerm.addTermValue(value);
        } else {
            TermType bookIndexTerm = createInstance(name, value);
            terms.insert(bookIndexTerm, name);
        }
    }


    public abstract TermType createInstance(String name, TermValue value);

    public void deleteTerm(String name) {
        terms.delete(name);
    }

    public void updateTerm(String oldName, String newName) {
        RedBlackNode<TermType, String> oldNode = terms.get(oldName);
        if (oldNode != null) {
            TermType oldBookIndexTerm = oldNode.getValue();
            TermType newBookIndexTerm = createInstance(newName, oldBookIndexTerm.getValue());
            terms.delete(oldName);
            terms.insert(newBookIndexTerm, newName);
        }
    }

    public void printTerms() {
        List<TermType> bookIndexTermValues = terms.inorder();
        for (TermType bookIndexTermValue : bookIndexTermValues) {
            System.out.println(bookIndexTermValue.toString());
        }
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        terms.inorder().forEach(x -> sb.append(x).append('\n'));
        sb.deleteCharAt(sb.length() - 1);
        return sb.toString();
    }

    public List<String> searchWordStartsWith(String starsWith) {
        return terms.inorder().stream().filter(x -> starsWithIgnoreCase(x.getTerm(), starsWith)).map(x -> x.getTerm()).collect(Collectors.toList());
    }

    private boolean starsWithIgnoreCase(String word, String starsWith) {
        return  word.toUpperCase().startsWith(starsWith.toUpperCase());
    }

    public boolean hasTerm(String term) {
        return terms.get(term) != null;
    }

    public TermType getTerm(String term) {
        return terms.get(term).getValue();
    }
}
