package org.fjala.book.glossary;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.jupiter.api.Assertions.*;

public class GlossaryTest {
    @Test
    void testAddTerm() {
        Glossary glossary = new Glossary();
        glossary.addTerm("Moon", "The natural satellite of any planet");
        glossary.addExample("Moon", "The current moon");

        assertTrue(glossary.hasTerm("Moon"));
        assertTrue(glossary.getExamples("Moon").contains("The current moon"));
    }

    @Test
    void testDeleteTerm() {
        Glossary glossary = new Glossary();
        glossary.addTerm("Moon", "The natural satellite of any planet");
        glossary.deleteTerm("Moon");
        assertFalse(glossary.hasTerm("Moon"));
    }

    //It's an extra scenario, example if you put wrong the name of the term you can update it.
    @Test
    void testUpdateTerm() {
        Glossary glossary = new Glossary();
        glossary.addTerm("moon", "The natural satellite of any planet");
        glossary.updateTerm("moon", "Moon");

        assertTrue(glossary.hasTerm("Moon"));
        assertFalse(glossary.hasTerm("moon"));
    }

    @Test
    void testUpdateDefinition() {
        Glossary glossary = new Glossary();
        glossary.addTerm("Moon", "The natural satellite of any planet");
        glossary.updateDefinition("Moon", "New Definition");
        
        assertEquals(glossary.getTerm("Moon").getDefinition(), "New Definition");
    }

    @Test
    void testGetTermsStarsWith() {
        Glossary glossary = new Glossary();
        glossary.addTerm("Term Moon", "The natural satellite of any planet");
        glossary.addTerm("Term 2", "Definition");
        glossary.addTerm("Term 3", "Definition");
        glossary.addTerm("Something", "Definition");

        assertThat(glossary.searchWordStartsWith("term"), containsInAnyOrder("Term Moon", "Term 2", "Term 3"));
    }

    @Test
    public void testPrint() {
        String expected = "Something: Definition.\n" +
                          "Term 1 Moon: The natural satellite of any planet.\n" +
                          "Term 2: Definition.\n" +
                          "Term 3: Definition. Examples: Example, Other Example";
        Glossary glossary = new Glossary();
        glossary.addTerm("Term 1 Moon", "The natural satellite of any planet.");
        glossary.addTerm("Term 2", "Definition.");
        glossary.addTerm("Term 3", "Definition.");
        glossary.addExample("Term 3", "Example");
        glossary.addExample("Term 3", "Other Example");

        glossary.addTerm("Something", "Definition.");

        glossary.printTerms();
        assertEquals(expected, glossary.toString());
    }

}
