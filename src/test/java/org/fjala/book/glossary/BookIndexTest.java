package org.fjala.book.glossary;

import org.junit.jupiter.api.Test;

import java.util.Collection;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

/*
 * My previous Book Index Test.
 */
class BookIndexTest {
    @Test
    void testInsertTerms() {
        BookIndex bookIndex = new BookIndex();

        bookIndex.addTerm("Arrays", 10);
        bookIndex.addTerm("Arrays", 20);
        bookIndex.addTerm("Binary Search", 30);

        assertThat(bookIndex.hasTerm("Arrays"), is(true));
        assertThat(bookIndex.hasTerm("Binary Search"), is(true));
        assertThat(bookIndex.getTerm("Arrays").getPages(), contains(10, 20));
        assertThat(bookIndex.getTerm("Binary Search").getPages(), contains(30));
    }

    @Test
    void testDeleteTerms() {
        BookIndex bookIndex = new BookIndex();
        bookIndex.addTerm("Arrays", 10);
        bookIndex.addTerm("Arrays", 20);
        bookIndex.addTerm("Binary Search", 30);

        bookIndex.deleteTerm("Arrays");

        assertThat(bookIndex.hasTerm("Arrays"), is(false));
    }

    @Test
    void testUpdateTerms() {
        BookIndex bookIndex = new BookIndex();
        bookIndex.addTerm("Arrays", 10);
        bookIndex.addTerm("Arrays", 20);
        bookIndex.addTerm("Binary Search", 30);

        bookIndex.updateTerm("Arrays", "Arrays Updated");
        assertThat(bookIndex.hasTerm("Arrays"), is(false));
        assertThat(bookIndex.hasTerm("Arrays Updated"), is(true));
        assertThat(bookIndex.getTerm("Arrays Updated").getPages(), contains(10, 20));
    }

    @Test
    void testGetAllTermsThatStarsWithAGivenWord() {
        BookIndex bookIndex = new BookIndex();
        bookIndex.addTerm("Arrays", 10);
        bookIndex.addTerm("Edges", 15);
        bookIndex.addTerm("Binary Search Tree", 20);
        bookIndex.addTerm("Binary Search", 30);

        Collection<String> matchedTerms = bookIndex.searchWordStartsWith("bin");

        assertThat(matchedTerms, containsInAnyOrder("Binary Search Tree", "Binary Search"));
    }

    @Test
    void testToStringOrderedAsPDFExample() {
        String expected = "Arrays: 20 - 23, 30\n" +
                          "Binary Search: 30, 40\n" +
                          "Binary Search Tree: 40 - 45\n" +
                          "Edges: 40, 53 - 55\n" +
                          "Graph: 50 - 57\n" +
                          "Nodes: 41 - 42, 53";
        BookIndex bookIndex = new BookIndex();
        bookIndex.addTerm("Arrays", 20);
        bookIndex.addTerm("Arrays", 21);
        bookIndex.addTerm("Arrays", 22);
        bookIndex.addTerm("Arrays", 23);
        bookIndex.addTerm("Arrays", 30);
        bookIndex.addTerm("Binary Search", 30);
        bookIndex.addTerm("Binary Search", 40);
        bookIndex.addTerm("Binary Search Tree", 40);
        bookIndex.addTerm("Binary Search Tree", 41);
        bookIndex.addTerm("Binary Search Tree", 42);
        bookIndex.addTerm("Binary Search Tree", 43);
        bookIndex.addTerm("Binary Search Tree", 44);
        bookIndex.addTerm("Binary Search Tree", 45);
        bookIndex.addTerm("Edges", 40);
        bookIndex.addTerm("Edges", 53);
        bookIndex.addTerm("Edges", 54);
        bookIndex.addTerm("Edges", 55);
        bookIndex.addTerm("Graph", 50);
        bookIndex.addTerm("Graph", 51);
        bookIndex.addTerm("Graph", 52);
        bookIndex.addTerm("Graph", 53);
        bookIndex.addTerm("Graph", 54);
        bookIndex.addTerm("Graph", 55);
        bookIndex.addTerm("Graph", 56);
        bookIndex.addTerm("Graph", 57);
        bookIndex.addTerm("Nodes", 41);
        bookIndex.addTerm("Nodes", 42);
        bookIndex.addTerm("Nodes", 53);

        assertThat(expected, equalTo(bookIndex.toString()));
    }
}
